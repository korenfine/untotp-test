import Vuelidate from 'vuelidate/src'

export default async ({ Vue }) => {
  Vue.use(Vuelidate)
}
