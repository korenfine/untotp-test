import Vue from 'vue'
import axios from 'axios'

const request = axios.create({
  headers: {
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
})

request.interceptors.response
  .use(
    (response) => {
      return response.data
    },
    (error) => {
      return Promise.reject(error.response)
    })

Vue.prototype.$axios = request
