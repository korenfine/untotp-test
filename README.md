# Install the dependencies
```bash
yarn

cd backend
yarn backend
```

### Build and Start the app
```bash
yarn prod
```
