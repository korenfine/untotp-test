require('dotenv').config()

const express = require('express')
const app = express()

const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser')
const http = require('http')

const port = process.env.PORT || 4000

app.use(cors({ origin: ['http://localhost:8080', 'http://localhost:8090'], credentials: true }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const server = http.createServer(app)

require('./routes')(app)

app.use(express.static(path.join(__dirname, '/../dist/spa/')))
app.get('*', (req, res) => { res.sendFile(path.resolve(path.join(__dirname, '/../dist/spa/index.html'))) })

server.listen(port, () => console.log(`Running on http://localhost:${port}`))
