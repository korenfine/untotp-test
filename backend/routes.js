const axios = require('axios')
const auth = {
  cid: process.env.ICOUNT_CID,
  user: process.env.ICOUNT_USER,
  pass: process.env.ICOUNT_PASS
}
const headers = {
  'content-type': 'application/json'
}

module.exports = (app) => {
  app.get('/api/clients/get', async (req, res) => {
    const { detailLevel } = req.query

    const data = { ...auth, detail_level: detailLevel }
    const response = await axios.get(process.env.ICOUNT_URL + '/client/get_list', { params: data, headers })

    const clients = response.data.clients

    res.send({ clients })
  })

  app.post('/api/clients/createUpdate', async (req, res) => {
    const { clientForm } = req.body

    const data = { ...auth, ...clientForm }
    const response = await axios.post(process.env.ICOUNT_URL + '/client/create_or_update', data, { headers })

    res.send({ client_id: response.data.client_id })
  })
}
